
import { useState } from "react";
import FormInput from "./components/FormInput";
import styled from "styled-components";

const universal = styled.div`
overflow:hidden;
`

const Appe = styled.div`
display: flex;
align-items: center;
justify-content: center;
height: 100vh;
background:url("/bg.jpg");
background-size: contain;
background-position: center;
`

const Form = styled.div`
background-color: rgb(22, 3, 86);
padding: 0px 60px;
border-radius: 10px;
`

const Titlee = styled.h1`  
color: rgb(77, 1, 77);
text-align: center;`

const Button = styled.button`
width: 100%;
height: 50px;
padding: 10px;
border: none;
background-color: rebeccapurple;
color: white;
border-radius: 5px;
font-weight: bold;
font-size: 18px;
cursor: pointer;
margin-top: 15px;
margin-bottom: 30px;
`


const App = () => {
  const [values, setValues] = useState({
    username: "",
    email: "",
    birthday: "",
    password: "",
    confirmPassword: "",
  });

  const inputs = [
    {
      id: 1,
      name: "username",
      type: "text",
      placeholder: "Username",
      errorMessage:
        "Username should be 3-16 characters and shouldn't include any special character!",
      label: "Username",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 2,
      name: "email",
      type: "email",
      placeholder: "Email",
      errorMessage: "It should be a valid email address!",
      label: "Email",
      required: true,
    },
    {
      id: 3,
      name: "birthday",
      type: "date",
      placeholder: "Birthday",
      label: "Birthday",
    },
    {
      id: 4,
      name: "password",
      type: "password",
      placeholder: "Password",
      errorMessage:
        "Password should be 8-20 characters and include at least 1 letter, 1 number and 1 special character!",
      label: "Password",
      pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
      required: true,
    },
    {
      id: 5,
      name: "confirmPassword",
      type: "password",
      placeholder: "Confirm Password",
      errorMessage: "Passwords don't match!",
      label: "Confirm Password",
      pattern: values.password,
      required: true,
    },
  ];

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  return (
    <universal>
      <Appe>
      <Form>
    <div className="app">
      <form onSubmit={handleSubmit}>
        <Titlee><h1>Register</h1></Titlee>
        {inputs.map((input) => (
          <FormInput
            key={input.id}
            {...input}
            value={values[input.name]}
            onChange={onChange}
          />
        ))}
        <Button button>Submit</Button>
      </form>
      
    </div>
    </Form>
    </Appe>
    </universal>
  );
};

export default App;